const db = require("../models");
const Libros = db.Libros;
const Op = db.Sequelize.Op;
exports.create = (req, res) => {
    // Validate request
    if (!req.body.codigo) {
        res.status(400).send({
            message: "Debe enviar el codigo del libro!"
        });
        return;
    }
    if (!req.body.titulo) {
        res.status(400).send({
            message: "Debe enviar el titulo del libro!"
        });
        return;
    }
    // crea un libro
    const libro = {
        titulo: req.body.titulo,
        autor: req.body.autor,
        descripcion: req.body.descripcion,
        codigo: req.body.codigo,
        fecha_publicacion: req.body.fecha_publicacion
    };
    // Guardamos a la base de datos
    Libros.create(libro)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear un libro."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Libros.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener libro con id=" + id
            });
        });
};

exports.findAll = (req, res) => {
    const nombre = req.query.nombre;
    var condition = nombre ? 
    { 
        titulo: { [Op.iLike]: `%${nombre}%` },
        autor: { [Op.iLike]: `%${nombre}%` },
        codigo: { [Op.iLike]: `%${nombre}%` }
     } : null;
     Libros.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener los libros."
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;
    Libros.destroy({ where: { id: id } })
        .then(() => {
            res.send({ message: 'Libro eliminada exitosamente.' });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al eliminar libro con id=" + id
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
    Libros.findByPk(id)
        .then(libro => {
            if (!libro) {
                return res.status(404).send({ message: "Libro no encontrado." });
            }

            // Actualiza los campos con los datos del cuerpo de la solicitud
            libro.titulo = req.body.titulo;
            console.log('libro.titulo', libro.titulo)
            libro.autor = req.body.autor;
            console.log('libro.autor', libro.autor)
            libro.descripcion = req.body.descripcion;
            console.log('libro.descripcion', libro.descripcion)
            libro.codigo = req.body.codigo;
            console.log('libro.codigo', libro.codigo)
            libro.fecha_publicacion = req.body.fecha_publicacion;
            console.log('libro.fecha_publicacion', libro.fecha_publicacion)

            // Guarda el libro actualizada
            libro.save()
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message: "Error al actualizar el libro con id=" + id
                    });
                });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener libro con id=" + id
            });
        });
};
