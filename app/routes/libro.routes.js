module.exports = app => {
    const libro = require("../controllers/librodao.controller.js");
    var router = require("express").Router();
    router.post("/", libro.create);
    router.get("/", libro.findAll);
    router.get("/:id", libro.findOne);

    // Agregar una ruta para la eliminación de libros por ID
    router.delete("/:id", libro.delete);

    // Agregar una ruta para la edición de libros por ID
    router.put("/:id", libro.update)
    app.use('/api/libro', router);
};