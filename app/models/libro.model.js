module.exports = (sequelize, Sequelize) => {
    const Libro = sequelize.define("Libro", {
        titulo: {
            type: Sequelize.STRING
        },
        autor: {
            type: Sequelize.STRING
        },
        descripcion: {
            type: Sequelize.STRING
        },
        codigo: {
            type: Sequelize.STRING
        },
        fecha_publicacion: {
            type: Sequelize.DATE
        },
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });
    return Libro;
};